package com.example.liveassistwithfcsdk.sample.util

class ListenerContainer<ListenerType> {
    private val listeners = mutableSetOf<ListenerType>()

    fun addListener(listener: ListenerType) {
        listeners.add(listener)
    }

    fun removeListener(listener: ListenerType) {
        listeners.remove(listener)
    }

    fun notifyListeners(block: (ListenerType) -> Unit) {
        for (listener in listeners) {
            block(listener)
        }
    }
}