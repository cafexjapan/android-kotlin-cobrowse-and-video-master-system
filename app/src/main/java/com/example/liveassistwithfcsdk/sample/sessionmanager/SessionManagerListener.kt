package com.example.liveassistwithfcsdk.sample.sessionmanager

interface SessionManagerListener {
    fun onCallStarted()
    fun onCallFailed()
    fun onCallEnded()
}