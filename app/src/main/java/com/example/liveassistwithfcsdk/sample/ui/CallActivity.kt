package com.example.liveassistwithfcsdk.sample.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.liveassistwithfcsdk.sample.databinding.ActivityCallBinding
import com.example.liveassistwithfcsdk.sample.permission.PermissionManager
import com.example.liveassistwithfcsdk.sample.sessionmanager.Configuration
import com.example.liveassistwithfcsdk.sample.sessionmanager.SessionManager
import com.example.liveassistwithfcsdk.sample.sessionmanager.SessionManagerListener
import org.koin.android.ext.android.inject

class CallActivity : AppCompatActivity(), SessionManagerListener {
    private lateinit var binding: ActivityCallBinding
    private val permissionManager by inject<PermissionManager>()
    private val sessionManager by inject<SessionManager>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCallBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.callButton.setOnClickListener {
            it.isEnabled = false
            permissionManager.requestRequiredPermissions(this)
            createOrEndCall()
        }

        binding.webButton.setOnClickListener {
            val nextIntent = Intent(this, WebActivity::class.java)
            startActivity(nextIntent)
        }
    }

    override fun onStart() {
        super.onStart()
        sessionManager.addListener(this)
        sessionManager.startSession(this)
    }

    override fun onResume() {
        super.onResume()

        binding.localVideo.startSmallViewAnimations()
        binding.remoteVideo.startLargeViewAnimations()
        sessionManager.remoteVideoView = binding.remoteVideo
        sessionManager.localVideoView = binding.localVideo
    }

    override fun onPause() {
        super.onPause()

        // Remove views to prevent multiple parents, these will be re-initialised on resume
        binding.localVideo.clearAnimation()
        binding.localVideo.removeAllViews()
        binding.remoteVideo.clearAnimation()
        binding.remoteVideo.removeAllViews()
    }

    override fun onStop() {
        super.onStop()
        sessionManager.removeListener(this)
    }

    private fun createOrEndCall() {
        if (sessionManager.hasCurrentCall) {
            sessionManager.endCall()
        } else {
            sessionManager.localVideoView = binding.localVideo
            sessionManager.remoteVideoView = binding.remoteVideo
            sessionManager.createCall(Configuration.gatewayCallee)
        }
    }

    private fun setCallStarted(isCallStarted: Boolean) {
        binding.callButton.isEnabled = true
        val buttonText = if (isCallStarted) "End Call" else "Make Call"
        binding.callButton.text = buttonText
    }

    // SessionManagerListener
    override fun onCallStarted() {
        setCallStarted(true)
    }

    override fun onCallFailed() {
        setCallStarted(false)
    }

    override fun onCallEnded() {
        setCallStarted(false)
    }
}
