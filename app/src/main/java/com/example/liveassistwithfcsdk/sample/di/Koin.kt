package com.example.liveassistwithfcsdk.sample.di

import com.example.liveassistwithfcsdk.sample.permission.PermissionManager
import com.example.liveassistwithfcsdk.sample.sessionmanager.RemoteAEDSessionTokenProvider
import com.example.liveassistwithfcsdk.sample.sessionmanager.SessionManager
import com.example.liveassistwithfcsdk.sample.sessionmanager.SessionTokenProvider
import com.example.liveassistwithfcsdk.sample.util.HttpUtil
import org.koin.dsl.module

val appModule = module {
    single { HttpUtil() }
    single<SessionTokenProvider> { RemoteAEDSessionTokenProvider(get()) }
    single { PermissionManager() }
    single { SessionManager(get()) }
}