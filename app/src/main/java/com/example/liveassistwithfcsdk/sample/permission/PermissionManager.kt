package com.example.liveassistwithfcsdk.sample.permission

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager

class PermissionManager {
    private val requiredPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.SYSTEM_ALERT_WINDOW
    )

    fun requestRequiredPermissions(activity: Activity) {
        val permissionsToRequest = requiredPermissions.filter {
            activity.checkSelfPermission(it) != PackageManager.PERMISSION_GRANTED
        }.toTypedArray()

        activity.requestPermissions(permissionsToRequest, 0)
    }
}