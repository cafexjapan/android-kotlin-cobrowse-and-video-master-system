package com.example.liveassistwithfcsdk.sample.ui

import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.example.liveassistwithfcsdk.sample.databinding.ActivityWebBinding
import com.example.liveassistwithfcsdk.sample.sessionmanager.SessionManager
import org.koin.android.ext.android.inject

class WebActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWebBinding
    private val sessionManager by inject<SessionManager>()

    private lateinit var remoteViewGroup: ViewGroup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWebBinding.inflate(layoutInflater)
        setContentView(binding.root)

        remoteViewGroup = binding.remoteVideo

        binding.webView.loadUrl("https://webrtc.org/")
    }

    override fun onStart() {
        super.onStart()
        sessionManager.remoteVideoView = remoteViewGroup
    }

    override fun onResume() {
        super.onResume()
        remoteViewGroup.startSmallViewAnimations()
        sessionManager.remoteVideoView = remoteViewGroup
    }

    override fun onPause() {
        super.onPause()

        // Remove views to prevent multiple parents, these will be re-initialised on resume
        remoteViewGroup.clearAnimation()
        remoteViewGroup.removeAllViews()
    }
}
