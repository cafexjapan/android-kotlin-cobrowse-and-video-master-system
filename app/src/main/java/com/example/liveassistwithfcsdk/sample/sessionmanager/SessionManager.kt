package com.example.liveassistwithfcsdk.sample.sessionmanager

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.ViewGroup
import com.alicecallsbob.assist.sdk.config.impl.AssistCobrowseListener
import com.alicecallsbob.assist.sdk.config.impl.AssistConfigBuilder
import com.alicecallsbob.assist.sdk.core.Assist
import com.alicecallsbob.assist.sdk.core.AssistError
import com.alicecallsbob.assist.sdk.core.AssistListener
import com.alicecallsbob.fcsdk.android.phone.*
import com.alicecallsbob.fcsdk.android.uc.UC
import com.alicecallsbob.fcsdk.android.uc.UCFactory
import com.alicecallsbob.fcsdk.android.uc.UCListener
import com.example.liveassistwithfcsdk.sample.SampleApplication
import com.example.liveassistwithfcsdk.sample.util.ListenerContainer
import com.example.liveassistwithfcsdk.sample.util.SampleTrustManager

class SessionManager(private val sessionTokenProvider: SessionTokenProvider) : UCListener,
    CallListener,
    AssistListener, AssistCobrowseListener {
    private val listeners = ListenerContainer<SessionManagerListener>()

    private var uc: UC? = null
    private var currentCall: Call? = null

    fun addListener(listener: SessionManagerListener) {
        listeners.addListener(listener)
    }

    fun removeListener(listener: SessionManagerListener) {
        listeners.removeListener(listener)
    }

    var localVideoView: ViewGroup? = null
        set(value) {
            field = value
            uc?.phone?.setPreviewView(value)
        }

    var remoteVideoView: ViewGroup? = null
        set(value) {
            field = value
            currentCall?.setVideoView(uc?.phone, value)
        }

    val hasCurrentCall: Boolean
        get() = currentCall != null

    fun startSession(context: Context) {
        if (uc !== null) {
            return;
        }

        sessionTokenProvider.provideSessionToken { sessionToken ->
            if (sessionToken != null) {
                uc = UCFactory.createUc(context, sessionToken, this)
                uc?.setTrustManager(SampleTrustManager())
                uc?.setNetworkReachable(true) // Naive assumption
                uc?.startSession()
            } else {
                listeners.notifyOnCallFailed()
            }
        }
    }

    fun createCall(destination: String) {
        if (this.uc === null) {
            listeners.notifyOnCallFailed();
        }

        currentCall = uc?.phone?.createCall(
            destination,
            MediaDirection.SEND_AND_RECEIVE,
            MediaDirection.SEND_AND_RECEIVE,
            this
        )

        Handler(Looper.getMainLooper()).post {
            uc?.phone?.setPreviewView(localVideoView)
            currentCall?.setVideoView(uc?.phone, remoteVideoView)
        }
    }

    fun endCall() {
        currentCall?.end()
        Handler(Looper.getMainLooper()).post {
            cleanUpViewsOnEnd(localVideoView, remoteVideoView)
        }
        currentCall = null;
    }

    private fun startCoBrowse() {
        sessionTokenProvider.provideSessionToken { sessionToken ->
            val context = SampleApplication.instance
            val config = AssistConfigBuilder(context)
                .setServerHost(Configuration.gatewayAddress)
                .setServerPort(Configuration.gatewayPort)
                .setConnectSecurely(Configuration.gatewayScheme == "https")
                .setTrustManager(SampleTrustManager())
                .setSessionToken(sessionToken)
                .setCorrelationId(Configuration.gatewayCaller)
                .setCobrowseListener(this)
                .build()
            Assist.startSupport(config, context, this)
        }
    }

    // UCListener
    override fun onSessionStarted() {}
    override fun onSessionNotStarted() {}
    override fun onSystemFailure() {}
    override fun onConnectionLost() {}
    override fun onConnectionRetry(p0: Int, p1: Long) {}
    override fun onConnectionReestablished() {}
    override fun onGenericError(p0: String?, p1: String?) {}

    // CallListener
    override fun onStatusChanged(call: Call?, status: CallStatus?) {
        Handler(Looper.getMainLooper()).post {
            onMainThreadStatusChanged(call, status)
        }
    }

    private fun onMainThreadStatusChanged(call: Call?, status: CallStatus?) {
        when (status) {
            CallStatus.IN_CALL -> {
                listeners.notifyOnCallStarted()
                startCoBrowse()
            }
            CallStatus.ENDED -> {
                endCall()
                Assist.endSupport()
                listeners.notifyOnCallEnded()
            }
            CallStatus.BUSY, CallStatus.NOT_FOUND, CallStatus.TIMED_OUT, CallStatus.ERROR -> {
                endCall()
                listeners.notifyOnCallFailed()
            }
            CallStatus.UNINITIALIZED -> {}
            CallStatus.SETUP -> {}
            CallStatus.ALERTING -> {}
            CallStatus.RINGING -> {}
            CallStatus.MEDIA_PENDING -> {}
            CallStatus.NO_MB_CAPACITY -> {}
            CallStatus.REQUEST_TERMINATED -> {}
            CallStatus.TEMPORARILY_UNAVAILABLE -> {}
            CallStatus.MEDIA_UNAVAILABLE -> {}
            null -> {}
        }
    }

    override fun onDialFailed(p0: Call?, p1: String?, p2: CallStatus?) {}
    override fun onCallFailed(p0: Call?, p1: String?, p2: CallStatus?) {}
    override fun onMediaChangeRequested(p0: Call?, p1: Boolean, p2: Boolean) {}
    override fun onStatusChanged(p0: Call?, p1: CallStatusInfo?) {}
    override fun onRemoteDisplayNameChanged(p0: Call?, p1: String?) {}
    override fun onRemoteMediaStream(p0: Call?) {}
    override fun onInboundQualityChanged(p0: Call?, p1: Int) {}
    override fun onRemoteHeld(p0: Call?) {}
    override fun onRemoteUnheld(p0: Call?) {}

    // AssistListener
    override fun onSupportEnded(p0: Boolean) {}
    @Deprecated("Deprecated in Java")
    override fun onSupportError(p0: AssistError?, p1: String?) {}

    // AssistCobrowseListener
    override fun onCobrowseActive() {}
    override fun onCobrowseInactive() {}

    private fun ListenerContainer<SessionManagerListener>.notifyOnCallStarted() =
        notifyListeners { it.onCallStarted() }

    private fun ListenerContainer<SessionManagerListener>.notifyOnCallFailed() =
        notifyListeners { it.onCallFailed() }

    private fun ListenerContainer<SessionManagerListener>.notifyOnCallEnded() =
        notifyListeners { it.onCallEnded() }
}