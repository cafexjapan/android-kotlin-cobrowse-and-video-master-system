package com.example.liveassistwithfcsdk.sample.sessionmanager

interface SessionTokenProvider {
    fun provideSessionToken(callback: (String?) -> Unit)
}