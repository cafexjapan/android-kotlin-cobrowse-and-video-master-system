package com.example.liveassistwithfcsdk.sample.util

import android.os.Handler
import android.os.Looper
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class HttpUtil {
    enum class Method(val string: String) {
        GET("GET"),
        POST("POST"),
        PUT("PUT")
    }

    fun makeJsonRequest(
        urlString: String,
        method: Method,
        body: Map<String, String>? = null,
        callback: (JSONObject?) -> Unit
    ) {
        val request: Request = Request.Builder()
            .url(urlString)
            .method(method.string, body?.asFormBody())
            .build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                invokeCallback(response.asJSONObject())
            }

            override fun onFailure(call: Call, e: IOException) {
                invokeCallback(null)
            }

            private fun invokeCallback(jsonObject: JSONObject?) {
                Handler(Looper.getMainLooper()).post {
                    callback(jsonObject)
                }
            }
        })
    }

    private fun Map<String, String>.asFormBody(): FormBody {
        return entries.fold(FormBody.Builder(), { acc, next ->
            acc.add(next.key, next.value)
        }).build()
    }

    private fun Response.asJSONObject(): JSONObject? {
        body?.let {
            return try {
                JSONObject(it.string())
            } catch (e: JSONException) {
                null
            }
        }
        return null
    }
}