package com.example.liveassistwithfcsdk.sample.ui

import android.view.View
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import com.example.liveassistwithfcsdk.sample.R

private fun View.startAnimations(ids: Array<Int>) {
    val animationSet = AnimationSet(false)
    ids.forEach {
        animationSet.addAnimation(AnimationUtils.loadAnimation(context, it))
    }
    startAnimation(animationSet)
}

fun View.startSmallViewAnimations() {
    startAnimations(
        arrayOf(
            R.anim.small_view_swaying,
            R.anim.small_view_pulsing
        )
    )
}

fun View.startLargeViewAnimations() {
    startAnimations(
        arrayOf(
            R.anim.large_view_swaying,
            R.anim.large_view_pulsing
        )
    )
}