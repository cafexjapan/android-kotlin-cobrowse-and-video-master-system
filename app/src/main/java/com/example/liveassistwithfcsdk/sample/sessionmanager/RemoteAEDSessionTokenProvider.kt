package com.example.liveassistwithfcsdk.sample.sessionmanager

import com.example.liveassistwithfcsdk.sample.util.HttpUtil

class RemoteAEDSessionTokenProvider(private val httpUtil: HttpUtil) : SessionTokenProvider {
    override fun provideSessionToken(callback: (String?) -> Unit) {
        val urlString = "${Configuration.authenticationUrl}&userId=${Configuration.gatewayCaller}"
        httpUtil.makeJsonRequest(urlString, HttpUtil.Method.GET) { jsonObject ->
            if (jsonObject != null) {
                callback(jsonObject.getString("sessionid"))
            } else {
                callback(null)
            }
        }
    }
}