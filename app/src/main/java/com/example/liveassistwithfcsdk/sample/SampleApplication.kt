package com.example.liveassistwithfcsdk.sample

import com.alicecallsbob.assist.sdk.core.AssistApplicationImpl
import com.example.liveassistwithfcsdk.sample.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class SampleApplication : AssistApplicationImpl() {
    companion object {
        lateinit var instance: SampleApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        startKoin {
            androidContext(this@SampleApplication)
            modules(appModule)
        }
    }
}