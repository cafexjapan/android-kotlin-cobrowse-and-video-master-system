package com.example.liveassistwithfcsdk.sample.sessionmanager

// Configuration is currently pulled from a file called `Secrets.kt` which defines the actual values.
// This file has not been included in the repository for security purposes.
//
// e.g.
//
//object Secrets {
//    val authenticationUrl = "https://your.authentication.address/path/to/service"
//    val gatewayScheme = "https"
//    val gatewayAddress = "your.gateway.address"
//    val gatewayPort = 8443
//    val gatewayCaller = "assist-anon"
//    val gatewayCallee = "agent1"
//}

object Configuration {
    val authenticationUrl = Secrets.authenticationUrl
    val gatewayScheme = Secrets.gatewayScheme
    val gatewayAddress = Secrets.gatewayAddress
    val gatewayPort = Secrets.gatewayPort
    val gatewayCaller = Secrets.gatewayCaller
    val gatewayCallee = Secrets.gatewayCallee
}