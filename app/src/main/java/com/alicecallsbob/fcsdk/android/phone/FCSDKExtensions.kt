package com.alicecallsbob.fcsdk.android.phone

import android.graphics.Point
import android.view.ViewGroup

/**
 * We maintain the video surfaces when switching between activities as this fails in onCreate
 * for subsequent calls. Only two video surfaces should be created during a call, one for remote,
 * one for local. They should be re-created for a subsequent call
 */
private var remoteSurface: VideoSurface? = null
private var localSurface: VideoSurface? = null

fun Phone.setPreviewView(viewGroup: ViewGroup?) {
    if (viewGroup != null) {
        if (localSurface == null) {
            localSurface = createVideoSurface(viewGroup)
            setPreviewView(localSurface)
        }
        viewGroup.removeAllViews()
        viewGroup.addView(localSurface)
    }
}

fun Call.setVideoView(phone: Phone?, viewGroup: ViewGroup?) {
    if (viewGroup != null) {
        if (remoteSurface == null) {
            remoteSurface = phone?.createVideoSurface(viewGroup)
            setVideoView(remoteSurface)
        }
        viewGroup.removeAllViews()
        viewGroup.addView(remoteSurface)
    }
}

/**
 * When a call has ended, remove the stored video surfaces for the old call so they are re-created
 * again when the new one begins, remove all views for the viewgroups used so that we are not left
 * with residual frames
 */
fun cleanUpViewsOnEnd(vararg viewGroups: ViewGroup?) {
    for (view in viewGroups) {
        view?.removeAllViews();
    }
    remoteSurface = null
    localSurface = null
}

private fun Phone.createVideoSurface(viewGroup: ViewGroup): VideoSurface {
    val surfaceSize = viewGroup.size()
    val videoSurface = createVideoSurface(viewGroup.context, surfaceSize, null)
    videoSurface.setZOrderOnTop(true)

    return videoSurface
}

private fun ViewGroup.size() = Point(width, height)
